
- lien du site : zoocraft.alwaysdata.net

- Connexion en tant qu'admin sur le site :  login = Pseudo1	
					    password = 123

-Dossier "zoocraft" du zip = racine du site

--------------- Critères respectés ou non : -------------------
- gitlab
* OK
- charte graphique au format PDF et à réutiliser dans les pages
* OK
- screenshots du premier site wordpress
* Non
- creer des page(s) WordPress avec des liens entre les différentes pages (au minimum page accueil + 3 categories + 3 produits par catégorie)
* OK dans le projet final mais pas faites avec wordpress
- creer des pages HTML + CSS (au moins la page d'accueil + connexion/inscription/ajout tickets/visualisation des tickets par l'administrateur) avec des liens vers le reste du site (pages WordPress)
* OK
- formulaire tickets
* OK
- Page index modifiée et maintenant dymanique en fonction de l'heure
* OK : changement du background en fonction des minutes (sur les pages : accueil, contacts et plan)
- inscription
* OK
- connexion
* OK
- sessions PHP (rester connecté)
* OK
- deconnexion
* OK
- creation ticket
* OK
- affichage liste de tous les tickets (admin connecté)
* OK
- affichage liste de tous les tickets émis (utilisateur connecté)
* OK
- affichage les détails d'1 seul ticket (admin connecté) + modifications / résolution possible
* OK
- mots de passe en BDD chiffrés correctement avec des aléas
* OK
- BDD (fournir le schéma complet des tables de la BDD (pas celles de WORDPRESS !!!) avec leurs relations)
* OK : zoocraft_bdd.png
- utiliser requetes asynchrones (instruction JS fetch) pour modifier les pages sans les recharger (ex1: mettre a jour la liste des tickets automatiquement quand on est sur la page qui les affiche tous / ou ex2 : changer les images de la page d'accueil toutes les 5 s)
* Non
- déployer sur un serveur distant (alwaysdata) et stockez un fichier du type README dans votre gitlab en indiquant l'adresse de votre site distant pour que je puisse le retrouver facilement!)
* OK
- tester les performances du site en utilisant au moins 2 analyseurs différents (les liens sont donnés dans le sujet) : et faire des captures d'écran à conserver (pas d'évaluation sur le résultat quantitatif des performances)
* OK : zoocraft_test1.png et zoocraft_test2.png 
- Realiser le diagramme des cas d'utilisation de votre site internet (use case diagram)
* OK : zoocraft_useCaseDiagram.png

